package jkcp;

import io.netty.buffer.ByteBuf;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Ikcpcb {
	
	/**conv为一个表示会话编号的整数，和tcp的 conv一样，通信双方需保证 conv相同，相互的数据包才能够被认可**/
	public  long conv;
	public  long mtu;
	/**单个包大小限制  用于把一个打包分成多个小包**/
	public  long mss;
	public  long state;
	
	
	
	/**最老的确认了的序列号。**/
	public  long snd_una;
	/**下一个发送的序列号**/
	public long snd_nxt;
	/**希望接收的下一个序列号**/
	public long rcv_nxt;
	public  long ts_recent, ts_lastack, ssthresh ;
	public  long rx_rttval, rx_srtt, rx_rto, rx_minrto ;
	public  long snd_wnd,rmt_wnd, cwnd, probe ;
	/**当前接收窗口的大小**/
	public long rcv_wnd;
	public  long current, ts_flush, xmit ;
	/**协议内部工作的 interval，单位毫秒，比如 10ms或者 20ms**/
	public long interval;
	
	
	
	
	
	/**收到的消息的大小  未排序好的**/
	public  long nrcv_buf;
	/**发送的消息队列的大小  未切分**/
	public long nsnd_buf;
	/**收到的消息的大小  排序好的**/
	public  long nrcv_que;
	/**发送的消息队列的大小  切分后的**/
	public  long nsnd_que;
	
	/**是否启用 nodelay模式，0不启用；1启用**/
	public  long nodelay;
	public  long updated ;
	public  long ts_probe, probe_wait ;
	public  long dead_link, incr ;
	
	/**收到的排序好的消息 排序好的**/
	public  LinkedList<IKCPSEG> rcv_queue;
	/**收到的消息集合  未排序**/
	public  LinkedList<IKCPSEG> rcv_buf;
	/**发送的消息队列  未切分**/
	public  LinkedList<IKCPSEG> snd_buf;
	/**发送的切分后的消息队列*/
	public  LinkedList<IKCPSEG> snd_queue;
	
	public  long[] acklist;
	public  long ackcount ;
	public  long ackblock ;
	public Object user;
	public ByteBuf buffer;
	/**快速重传模式，默认0关闭，可以设置2（2次ACK跨越将会直接重传）**/
	public  int fastresend;
	/**是否关闭流控，默认是0代表不关闭，1代表关闭。**/
	public  int nocwnd;
	public  int logmask;
	/**写数据的地方**/
	public IOutput iOutPut;
}
