package jkcp;

import io.netty.buffer.ByteBuf;

public interface IOutput {
	public int Output(ByteBuf buf,int _len,Ikcpcb kcp,Object user);
}
