package jkcp;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.PooledByteBufAllocator;

import java.nio.ByteOrder;

public class DelayPacket {
	public ByteBuf _ptr;
	public int _size;
	public int _ts;

	public void Init(int size, ByteBuf src) {
		_ptr = PooledByteBufAllocator.DEFAULT.directBuffer(size);
		_ptr.order(ByteOrder.LITTLE_ENDIAN);
		this._size = size;
		src.markReaderIndex();
		src.readBytes(_ptr, size);
		src.resetReaderIndex();
	}

	public ByteBuf get_ptr() {
		return _ptr;
	}

	public void set_ptr(ByteBuf _ptr) {
		this._ptr = _ptr;
	}

	public int get_size() {
		return _size;
	}

	public void set_size(int _size) {
		this._size = _size;
	}

	public int get_ts() {
		return _ts;
	}

	public void set_ts(int _ts) {
		this._ts = _ts;
	}
	// func (p *DelayPacket) Init(size int, src []byte) {
	// p._ptr = make([]byte, size)
	// p._size = size
	// copy(p._ptr, src[:size])
	// }LatencySimulator
	// func (p *DelayPacket) ts() int32 { return p._ts }
	// func (p *DelayPacket) setts(ts int32) { p._ts = ts}

}
